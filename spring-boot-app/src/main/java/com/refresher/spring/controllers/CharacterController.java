package com.refresher.spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahesh on 18-05-2017.
 */

@RestController
public class CharacterController {

    List<String> list;

    public CharacterController() {
        this.list = new ArrayList<>();

        this.list.add("Mahesh1");
        this.list.add("Mahesh2");
        this.list.add("Mahesh3");
    }

    @RequestMapping("/characters")
    List<String> characters() {
        return this.list;
    }

    @RequestMapping(value = "/characters/{id}", method = RequestMethod.DELETE)
    /*@ResponseStatus(HttpStatus.NO_CONTENT)*/
    void delete(@PathVariable("id") Integer id) {
        if(id < this.list.size() -1) {
            this.list.remove(id);
        } else {
            throw new RuntimeException("Current list size is " + (this.list.size() - 1));
        }

    }

    // more controller methods
}
